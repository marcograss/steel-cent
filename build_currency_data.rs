// Copyright 2016 John D. Hume
//
// Licensed under the Apache License, Version 2.0, <LICENSE-APACHE or
// http://apache.org/licenses/LICENSE-2.0> or the MIT license <LICENSE-MIT or
// http://opensource.org/licenses/MIT>, at your option. This file may not be
// copied, modified, or distributed except according to those terms.

extern crate csv;

use std::env;
use std::fs::File;
use std::io::Write;
use std::path::Path;

fn main() {
    let out_dir = env::var("OUT_DIR").unwrap();
    let dest_path = Path::new(&out_dir).join("generated_currency_data");
    let mut f = File::create(&dest_path).unwrap();
    let mut reader = csv::Reader::from_reader(File::open("./MoneyData.csv").unwrap());
    let records = reader.records();
    let mut records_vec = Vec::new();
    for r in records {
        records_vec.push(r.unwrap());
    }
    for r in records_vec.clone() {
        let code = r[0].to_owned();
        let num_code: i64 = r[1].parse().unwrap();
        let dec_places: i64 = r[2].parse().unwrap();
        if dec_places < 0 || code.starts_with('#') {
            continue;
        }
        f.write_all(
            format!("pub const {}: Currency = Currency {{ code_base26: {}, numeric_code_and_decimal_places: {}{} }};\n",
                code, code_to_base26(code.as_str()), num_code, dec_places)
                .as_bytes())
            .unwrap();
    }
    f.write_all(
        br"lazy_static!{
        static ref CURRENCIES_BY_CODE: HashMap<String, Currency> = {
        let mut m = HashMap::new();
        ",
    )
    .unwrap();
    for r in records_vec.clone() {
        let code = r[0].to_owned();
        let dec_places: i64 = r[2].parse().unwrap();
        if dec_places < 0 || code.starts_with('#') {
            continue;
        }
        f.write_all(format!("m.insert({code}.code(), {code});").as_bytes())
            .unwrap();
    }
    f.write_all(
        br"
        m
        };

        static ref CURRENCIES_BY_NUMERIC_CODE: HashMap<u16, Currency> = {
        let mut m = HashMap::new();
        ",
    )
    .unwrap();
    for r in records_vec.clone() {
        let code = r[0].to_owned();
        let dec_places: i64 = r[2].parse().unwrap();
        if dec_places < 0 || code.starts_with('#') {
            continue;
        }
        f.write_all(format!("m.insert({code}.numeric_code(), {code});").as_bytes())
            .unwrap();
    }
    f.write_all(
        br"m
        };
        }",
    )
    .unwrap();
}

// TODO find a way to share this with the prod base26 module
const A_OFFSET: u8 = 65;
const CODE_POS_1_MULTIPLIER: u16 = 26 * 26;
const CODE_POS_2_MULTIPLIER: u16 = 26;

fn code_to_base26(code: &str) -> u16 {
    assert_eq!(3, code.len(), "Code must be three upper-case letters");
    let bytes = code.as_bytes();
    for b in bytes {
        assert!(A_OFFSET <= *b && *b <= (A_OFFSET + 26));
    }
    (u16::from(bytes[0] - A_OFFSET) * CODE_POS_1_MULTIPLIER)
        + (u16::from(bytes[1] - A_OFFSET) * CODE_POS_2_MULTIPLIER)
        + u16::from(bytes[2] - A_OFFSET)
}
