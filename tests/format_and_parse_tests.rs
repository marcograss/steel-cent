#[macro_use]
extern crate quickcheck;

extern crate steel_cent;

use std::fmt::Debug;
use steel_cent::currency::{GBP, USD};
use steel_cent::{currency, formatting, Money, SmallMoney};

quickcheck! {
    fn round_trip_format_then_parse_usd_in_us_style(minor_amount: i64) -> bool {
        let style = formatting::us_style();
        let money = Money::of_minor(USD, minor_amount);
        let formatted_money = formatting::format(style, &money);
        Ok(money) == style.parser().parse(&formatted_money)
    }

    fn round_trip_format_then_parse_gbp_in_us_style(minor_amount: i64) -> bool {
        let style = formatting::us_style();
        let money = Money::of_minor(GBP, minor_amount);
        let formatted_money = formatting::format(style, &money);
        Ok(money) == style.parser().parse(&formatted_money)
    }
}

#[test]
fn round_trip_format_then_parse_money_of_every_length_in_every_style() {
    fn assert_formatted_money_parses_to_same_value<T>(style: &formatting::FormatSpec, money: T)
    where
        T: Eq + Debug + formatting::FormattableMoney + formatting::ParseableMoney,
    {
        let formatted_money = formatting::format(style, &money);
        assert_eq!(Ok(money), style.parser().parse(&formatted_money));
    }
    for style in &[
        formatting::us_style(),
        formatting::uk_style(),
        formatting::france_style(),
        formatting::generic_style(),
    ] {
        for currency in &[
            currency::AUD,
            currency::CHF,
            currency::EUR,
            currency::GBP,
            currency::JPY,
            currency::MXN,
            currency::NZD,
            currency::CAD,
            currency::USD,
        ] {
            for s in substrings::starting_at_length(1, i64::max_value())
                .chain(substrings::starting_at_length(2, i64::min_value()))
            {
                let minor_amount: i64 = s.parse().unwrap();
                assert_formatted_money_parses_to_same_value(
                    style,
                    Money::of_minor(*currency, minor_amount),
                );
            }
            for s in substrings::starting_at_length(1, i32::max_value())
                .chain(substrings::starting_at_length(2, i32::min_value()))
            {
                let minor_amount: i32 = s.parse().unwrap();
                assert_formatted_money_parses_to_same_value(
                    style,
                    SmallMoney::of_minor(*currency, minor_amount),
                );
            }
        }
    }
}

mod substrings {
    use std::fmt::Display;

    pub fn starting_at_length<T: Display>(len: usize, of_s: T) -> Substrs {
        Substrs {
            s: format!("{of_s}"),
            len,
        }
    }

    pub struct Substrs {
        s: String,
        len: usize,
    }

    impl Iterator for Substrs {
        type Item = String;
        fn next(&mut self) -> Option<Self::Item> {
            let len = self.len;
            if len <= self.s.len() {
                self.len += 1;
                Some(self.s[0..len].to_string())
            } else {
                None
            }
        }
    }

    #[test]
    fn substrs() {
        let mut substrs = starting_at_length(1, "abc");
        assert_eq!(Some("a".to_string()), substrs.next());
        assert_eq!(Some("ab".to_string()), substrs.next());
        assert_eq!(Some("abc".to_string()), substrs.next());
        assert_eq!(None, substrs.next());
        substrs = starting_at_length(2, "abc");
        assert_eq!(Some("ab".to_string()), substrs.next());
        assert_eq!(Some("abc".to_string()), substrs.next());
        assert_eq!(None, substrs.next());
    }
}
