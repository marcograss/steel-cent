steel-cent, currency and money values for Rust
==========

See examples, docs, etc. at [duelinmarkers.gitlab.io/steel-cent](http://duelinmarkers.gitlab.io/steel-cent/) or just browse the rustdocs in the source.

Features
--------

* Two value types for monetary amounts: `Money` and `SmallMoney`
* A value type for currencies: `Currency`
* Customizable output formatting and input parsing of money values
* Currency lookup by ISO 4217 alpha (e.g., "USD") or numeric (e.g, 840) codes


TODO
----

* Should checked_{add,sub} return None on mismatched currency instead of panicking?
* Look up currency by country code
* Support omitting zero minor part in FormatSpec
* Accept Into<Money> in Money ops (maybe)
* Support for adding custom currencies to lookup table
* Include pseudo-currencies w/o decimal places (not sure how that should work)
* Include historical currencies (maybe)
* Include non-standard (crypto-)currencies (maybe)
* BigMoney (not limited to minor-unit precision) backed by BigInt
* Is there a better way to do pre-defined FormatSpecs?
* meta: Unify README w/ site/index.html
* meta: Customize rustdocs


Joda
----

The design of steel-cent is largely inspired by the excellent [Joda Money](http://www.joda.org/joda-money/)
Java library. It also uses [Joda Money's currency data](https://github.com/JodaOrg/joda-money/blob/master/src/main/resources/org/joda/money/MoneyData.csv).


Legal
-----

steel-cent is Copyright 2016 John D. Hume.

### License

Licensed under either of

 * Apache License, Version 2.0, ([LICENSE-APACHE](LICENSE-APACHE) or http://www.apache.org/licenses/LICENSE-2.0)
 * MIT license ([LICENSE-MIT](LICENSE-MIT) or http://opensource.org/licenses/MIT)

at your option.

### Contribution

Unless you explicitly state otherwise, any contribution intentionally submitted
for inclusion in the work by you, as defined in the Apache-2.0 license, shall have its
*copyright assigned to John D. Hume* and be *dual licensed as above*, keeping it free for you
and anyone else to use, without any additional terms or conditions.
